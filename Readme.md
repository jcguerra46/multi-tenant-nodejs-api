# README #

### [WIP] Multi-tenant NodeJS API ###
With this app you can create companies and you can create users with roles and permissions for each company. 

- Users from one company will not be able to see other users for other companies.
- The articles of a company will not be able to see for other companies.

## Run the application
```sh
$ npm start
```

## Create MySQL Database
You need to create your MySQL databases for each environment and then put the databases credentials in the .env file.

## Migrations
You need to run this command to generate the migrations
```sh
$ npm run migrate
```

## Seeders
You need to run this command to generate the seeders
```sh
$ npm run seed
```

## Swagger Documentation
First, you need to run the application and then go to the following link in your browser
```sh
ex: http://localhost:3001/api-docs/
```

## Author
Juan Carlos Guerra 
```sh
email: juancarlosguerra46@gmail.com
```