const express = require('express');
const bodyParser = require('body-parser');
const passport = require("passport");
require('dotenv').config();

// Required routes
var indexRouter = require('./src/routes/index');

// Swagger Documentation
const swaggerUi = require('swagger-ui-express');
const { swaggerSpect } = require('./src/config/swagger');

const app = express();

// Globals variables
const APP_PORT = process.env.APP_PORT || 3000;

// Parsing application/json
app.use(bodyParser.json());

// Parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpect));

// Passport initialize and set middleware
app.use(passport.initialize());
require("./src/middlewares/jwt")(passport);

// Declared routes
app.use('/', indexRouter);

app.listen(APP_PORT,()=> {
    console.log(`The server is working on http://localhost:${APP_PORT}`)
})

module.exports = app;