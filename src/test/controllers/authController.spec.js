const app = require('../../../app');
const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);

describe("Authentication Test", () => {

    let token;
    beforeEach(() => {
        return new Promise((resolve, reject) => {
            const loginUser = {
                email: "admin@war.com",
                password: "password",
            };

            chai.request(app)
                .post("/auth/login")
                .set("Accept", "application/json")
                .send(loginUser)
                .end((err, res) => {
                    token = res.body.data.token; // save the token!
                    resolve();
                });
        });
    });

    it('It should get a user with authorization token', (done) => {
        chai.request(app)
        .get("/users/4")
        .set("Authorization", `Bearer ${token}`)
        .end((err, res) => {
            console.log(res.body.data.firstName);
            expect(res).to.have.status(202);
            expect(res.body).to.have.property("status");
            expect(res.body.status).is.equal("success");
            expect(res.body).to.have.property("message");
            expect(res.body.message).is.equal(`Accepted`);
            expect(res.body.data).to.have.property("id");
            expect(res.body.data).to.have.property("firstName");
            expect(res.body.data).to.have.property("lastName");
            expect(res.body.data).to.have.property("email");
            expect(res.body.data).to.have.property("emailVerifiedAt");
            done();
        });
        
    });

    it('It should not get a user with invalid email', (done) => {
        const invalidEmail = {
            email: 'invalidemail@war.com',
            password: 'password'
        }
        chai.request(app)
        .post("/auth/login")
        .set("Accept", "application/json")
        .send(invalidEmail)
        .end((err, res) => {
            expect(res).to.have.status(404);
            expect(res.body).to.have.property("status");
            expect(res.body.status).is.equal('error');
            expect(res.body).to.have.property("message");
            expect(res.body.message).is.equal(`The invalid email or is not associated with a user.`);
            done();
        });
        
    });

    it('It should not get a user with invalid password', (done) => {
        const invalidPassword = {
            email: 'admin@war.com',
            password: 'invalidPassword'
        }
        chai.request(app)
            .post('/auth/login')
            .set('Accept', 'application/json')
            .send(invalidPassword)
            .end((err, res) => {
                expect(res).to.have.status(404);
                expect(res.body).to.have.property("status");
                expect(res.body.status).is.equal("error");
                expect(res.body).to.have.property("message");
                expect(res.body.message).is.equal(`Incorrect password`);

                done();
            });
    });

    it('It should not get a user with password less than 8 character', (done) => {
        const invalidPassword = {
            email: 'admin@war.com',
            password: '1234567'
        }
        chai.request(app)
            .post('/auth/login')
            .set('Accept', 'application/json')
            .send(invalidPassword)
            .end((err, res) => {
                expect(res).to.have.status(422);
                expect(res.body).to.have.property("error");
                expect(res.body.error).to.have.property("password");
                expect(res.body.error.password).to.have.property("msg");
                expect(res.body.error.password.msg).is.equal('Must be at least 8 chars long');
                done();
            });
    });

});