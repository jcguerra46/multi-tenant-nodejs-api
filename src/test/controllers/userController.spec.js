
const app = require('../../../app');
const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);

describe("Users", () => {

    let token;
    beforeEach(() => {
        return new Promise((resolve, reject) => {
            const loginUser = {
                email: "admin@war.com",
                password: "password",
            };

            chai.request(app)
                .post("/auth/login")
                .set("Accept", "application/json")
                .send(loginUser)
                .end((err, res) => {
                    token = res.body.data.token; // save the token!
                    resolve();
                });
        });
    });

    describe('#index', () => {
        it('It should get a list of user', (done) => {
            chai.request(app)
            .get("/users/")
            .set("Authorization", `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.have.property("status");
                expect(res.body.status).is.equal("success");
                expect(res.body).to.have.property("message");
                expect(res.body.message).is.equal(`OK`);
                expect(res.body).to.have.property("data");
                expect(res.body.data).is.a("array");
                done();
            });
            
        });
    });

    describe('#store', () => {
        it("should insert a User", (done) => {
            chai
              .request(app)
              .post("/users")
              .set("Authorization", `Bearer ${token}`)
              .send({
                  "firstName" : "FirstName test",
                  "lastName" : "LastName test",
                  "email": "emailtest@war.com",
                  "password": "password",
                  "passwordConfirmation": "password",
                  "companyId": 4,
                  "avatar": "image.jpg"
              })
              .end((err, res) => {
                expect(res).to.have.status(201);
                expect(res.body).to.have.property("status");
                expect(res.body.status).is.equal("success");
                expect(res.body).to.have.property("data");
                expect(res.body.data).to.have.property("firstName");
                expect(res.body.data.firstName).is.equal("FirstName test");
                expect(res.body.data).to.have.property("lastName");
                expect(res.body.data.lastName).is.equal("LastName test");
                expect(res.body.data).to.have.property("companyId");
                expect(res.body.data.companyId).is.equal(4);
                done();
              });
          });
    });

    describe('#show', () => {
        it('It should get a user', (done) => {
            chai.request(app)
            .get("/users/5")
            .set("Authorization", `Bearer ${token}`)
            .end((err, res) => {
                expect(res).to.have.status(202);
                expect(res.body).to.have.property("status");
                expect(res.body.status).is.equal("success");
                expect(res.body).to.have.property("message");
                expect(res.body.message).is.equal(`Accepted`);
                expect(res.body.data).to.have.property("id");
                expect(res.body.data).to.have.property("firstName");
                expect(res.body.data).to.have.property("lastName");
                expect(res.body.data).to.have.property("email");
                expect(res.body.data).to.have.property("emailVerifiedAt");
                done();
            });
            
        });
    });

    describe('#update', () => {
        it("should edit an User", (done) => {
            chai
              .request(app)
              .put("/users/5")
              .set("Authorization", `Bearer ${token}`)
              .send({
                "firstName": "First Name Changed",
                "lastName": "Last Name Changed"
              })
              .end((err, res) => {
                expect(res).to.have.status(202);
                expect(res.body).to.have.property("status");
                expect(res.body.status).is.equal("success");
                expect(res.body).to.have.property("data");
                expect(res.body.data).to.have.property("id");
                expect(res.body.data).to.have.property("firstName");
                expect(res.body.data).to.have.property("lastName");
                expect(res.body.data).to.have.property("email");
                expect(res.body.data).to.have.property("password");
                expect(res.body.data).to.have.property("avatar");
                expect(res.body.data).to.have.property("companyId");
                expect(res.body.data.firstName).is.equal("First Name Changed");
                expect(res.body.data.lastName).is.equal("Last Name Changed");
        
                done();
              });
        });
    });

    describe('#delete', () => {
        it("should delete an User", (done) => {
            chai
                .request(app)
                .delete("/users/6")
                .set("Authorization", `Bearer ${token}`)
                .end((err, res) => {
                    expect(res).to.have.status(204);
        
                    done();
                });
        });
    });

});