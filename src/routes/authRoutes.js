const express = require('express');

const AuthController = require('../controllers/authController');
const { loginRulesAuth } = require('../validations/authRules');
const validationResult = require('../middlewares/validationResult');

const router = express.Router();

/**
 * @swagger
 * /auth/login:
 *   post:
 *     tags: [Authentication]
 *     description: Authentication login
 *     requestBody:
 *      required: true
 *      content:
 *          application/x-www-form-urlencoded:
 *              schema:
 *                  type: object
 *                  properties:
 *                      email:
 *                          type: string
 *                      password:
 *                          type: string
 *                  required:
 *                      - email
 *                      - password
 *     responses:
 *       200:
 *         description: token
 */
router.post("/login",
    loginRulesAuth(),
    validationResult,
    async (req, res) => (new AuthController).login(req, res)
);

module.exports = router;