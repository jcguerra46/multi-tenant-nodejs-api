const express = require("express");
const router = express.Router();

const homeRouter = require('./homeRoutes');
const authRouter = require('./authRoutes');
const userRouter = require('./userRoutes');

// <<<<----- <> ----->>>>>

router.use('/', homeRouter);
router.use('/auth', authRouter);
router.use('/users', userRouter);

module.exports = router;