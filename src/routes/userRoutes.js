const express = require('express');

const userController = require('../controllers/userController');
const validationResult = require('../middlewares/validationResult');
const { userStoreRules, userUpdateRules } = require('../validations/userRules');
const authenticate = require('../middlewares/authenticate');

const router = express.Router();

/**
 * Index route
 */
/**
 * @swagger
 * /users:
 *   get:
 *     tags: [Users]
 *     description: Returns a list of users.
 *     security:
 *          - BearerAuth: []
 *     responses:
 *        200:
 *            description: List of Users.
 *            content:
 *              application/json:
 *                schema:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: integer
 *                      firstName:
 *                        type: string
 *                      lastName:
 *                          type: string
 *                      email:
 *                          type: string
 *                      emailVerifiedAt:
 *                          type: date
 *                      password:
 *                          type: string
 *                      avatar:
 *                          type: string
 *                      status:
 *                          type: boolean
 *                      companyId:
 *                          type: integer
 *                      createdAt:
 *                          type: date
 *                      updatedAt:
 *                          type: date
 */
router.get(
    '/', 
    authenticate,
    async (req, res) => (new userController).index(req, res)
);

/**
 * Post route
 */
/**
 * @swagger
 * /users:
 *   post:
 *     tags: [Users]
 *     description: Store a user
 *     requestBody:
 *      required: true
 *      content:
 *          application/x-www-form-urlencoded:
 *              schema:
 *                  type: object
 *                  properties:
 *                      firstName:
 *                          type: string
 *                      lastName:
 *                          type: string
 *                      email:
 *                          type: string
 *                      password:
 *                          type: string
 *                      passwordConfirmation:
 *                          type: string
 *                      avatar:
 *                          type: string
 *                      companyId:
 *                          type: integer
 *                      status:
 *                          type: boolean
 *                  required:
 *                      - firstName
 *                      - lastName
 *                      - email
 *                      - passwordConfirmation
 *                      - password
 *                      - companyId
 *     security:
 *          - BearerAuth: []
 *     responses:
 *        201:
 *            description: User object.
 *            content:
 *              application/json:
 *                schema:
 *                  type: object
 *                  properties:
 *                    id:
 *                      type: integer
 *                    firstName:
 *                      type: string
 *                    lastName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    emailVerifiedAt:
 *                      type: date
 *                    avatar:
 *                      type: string
 *                    companyId:
 *                      type: integer
 *                    status:
 *                        type: boolean
 *                    createdAt:
 *                        type: date
 *                    updatedAt:
 *                        type: date
 */
router.post(
    '/',
    authenticate,
    userStoreRules(),
    validationResult,
    async (req, res) => (new userController).store(req, res) 
);

/**
 * Show route
 */
/**
 * @swagger
 * /users/{id}:
 *   get:
 *     tags: [Users]
 *     description: Returns a user.
 *     parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *            required: true
 *            description: id to get a user.
 *     security:
 *          - BearerAuth: []
 *     responses:
 *        200:
 *            description: User object.
 *            content:
 *              application/json:
 *                schema:
 *                  type: object
 *                  properties:
 *                    id:
 *                      type: integer
 *                    firstName:
 *                      type: string
 *                    lastName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    emailVerifiedAt:
 *                      type: date
 *                    avatar:
 *                      type: string
 *                    companyId:
 *                      type: integer
 *                    status:
 *                        type: boolean
 *                    createdAt:
 *                        type: date
 *                    updatedAt:
 *                        type: date
 *        404:
 *            description: User Not Found
 */
router.get(
    '/:id',
    authenticate,
    async (req, res) => (new userController).show(req, res)
);

/**
 * Update route
 */
/**
 * @swagger
 * /users/{id}:
 *   put:
 *     tags: [Users]
 *     description: Update a user
 *     parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *            required: true
 *            description: id to get a user.
 *     requestBody:
 *      required: true
 *      content:
 *          application/x-www-form-urlencoded:
 *              schema:
 *                  type: object
 *                  properties:
 *                      firstName:
 *                        type: string
 *                      lastName:
 *                        type: string
 *                      email:
 *                        type: string
 *                      password:
 *                        type: string
 *                      avatar:
 *                         type: string
 *                      companyId:
 *                         type: integer
 *                      status:
 *                          type: boolean
 *     security:
 *          - BearerAuth: []
 *     responses:
 *        200:
 *            description: User object.
 *            content:
 *              application/json:
 *                schema:
 *                  type: object
 *                  properties:
 *                    id:
 *                      type: integer
 *                    firstName:
 *                      type: string
 *                    lastName:
 *                      type: string
 *                    email:
 *                      type: string
 *                    emailVerifiedAt:
 *                      type: date
 *                    avatar:
 *                      type: string
 *                    companyId:
 *                      type: integer
 *                    status:
 *                        type: boolean
 *                    createdAt:
 *                        type: date
 *                    updatedAt:
 *                        type: date
 *        404:
 *            description: User Not Found
 */
router.put(
    '/:id',
    authenticate,
    userUpdateRules(),
    validationResult,
    async (req, res) => (new userController).update(req, res) 
    
);

/**
 * Delete route
 */
/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     tags: [Users]
 *     description: Delete a user
 *     parameters:
 *          - in: path
 *            name: id
 *            schema:
 *              type: integer
 *            required: true
 *            description: id to get a user.
 *     security:
 *          - BearerAuth: []
 *     responses:
 *       204:
 *         description: No content
 */
router.delete(
    '/:id',
    authenticate,
    async (req, res) => (new userController).destroy(req, res)
);

module.exports = router;