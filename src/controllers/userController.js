const db = require('../models/index');
const bcrypt = require('bcrypt');
const ApiController = require('../utilities/ApiController');
const HttpResponse = require("../utilities/HttpResponse");
const Response = new HttpResponse();

class userController extends ApiController {

    /**
     * @route GET /users
     * @desc Display a listing of the resource.
     * 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    async index(req, res) {
        await db.User.findAll().then((users) => {
            this.setSuccess(Response.HTTP_OK, Response.statusText.HTTP_OK, users);
            return this.send(res);
        }).catch(() => {
            this.setError(Response.HTTP_UNPROCESSABLE_ENTITY, error.message);
            return this.send(res);
        });
    };

    /**
     * @route POST /users
     * @desc Store a newly created resource in storage.
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async store(req, res) {
        await db.User.create({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password:  bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null),
            avatar: req.body.avatar,
            companyId: req.body.companyId
        }).then((userCreated) => {
            this.setSuccess(Response.HTTP_CREATED, Response.statusText.HTTP_CREATED, userCreated);
            return this.send(res);
        }).catch((error) => {
            this.setError(Response.HTTP_UNPROCESSABLE_ENTITY, { errors: error.message });
            return this.send(res);
        });
    };

    /**
     * @route GET /users/{id}
     * @desc Returns a specific user
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async show(req, res) {
        await db.User.findOne({
            where: { id: req.params.id }
        }).then((user) => {
            if(user) {
                this.setSuccess(Response.HTTP_ACCEPTED, Response.statusText.HTTP_ACCEPTED, user);
                return this.send(res);
            }
            this.setError(Response.HTTP_NOT_FOUND, `User ${Response.statusText.HTTP_NOT_FOUND}`);
            return this.send(res);
        }).catch(() => {
            this.setError(Response.HTTP_NOT_FOUND, `User ${Response.statusText.HTTP_NOT_FOUND}`);
            return this.send(res);
        })
    };

    /**
     * @route PUT /users/{id}
     * @desc Update the specified resource in storage.
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async update(req, res) {
        await db.User.findOne(
            { where: { id: req.params.id }
        }).then(async (user) => {
            user.firstName = req.body.firstName ? req.body.firstName : user.firstName;
            user.lastName = req.body.lastName ? req.body.lastName : user.lastName;
            user.email = req.body.email ? req.body.email : user.email;
            if(req.body.password){
                user.password = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null);
            }
            user.avatar = req.body.avatar ? req.body.avatar : user.avatar;
            user.save();
            this.setSuccess(Response.HTTP_ACCEPTED, Response.statusText.HTTP_ACCEPTED, user);
            return this.send(res);
        }).catch(() => {
            this.setError(Response.HTTP_NOT_FOUND, Response.statusText.HTTP_NOT_FOUND);
            return this.send(res);
        });
    };

    /**
     * @route DELETE /users/{id}
     * @desc Remove the specified resource from storage.
     * 
     * @param {*} req 
     * @param {*} res 
     */
    async destroy(req, res) {
        await db.User.destroy(
            { where: { id: req.params.id }
        }).then(() => {
            this.setSuccess(Response.HTTP_NO_CONTENT, Response.statusText.HTTP_NO_CONTENT, {});
            return this.send(res);
        }).catch(() => {
            this.setError(Response.HTTP_NOT_FOUND, Response.statusText.HTTP_NOT_FOUND);
            return this.send(res);
        });
    };

}

module.exports = userController;