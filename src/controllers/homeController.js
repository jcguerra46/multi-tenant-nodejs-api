const ApiController = require('../utilities/ApiController');
const HttpResponse = require("../utilities/HttpResponse");
const Response = new HttpResponse();

class homeController extends ApiController {

    /**
     * @route GET /
     * @desc Display a listing of the resource.
     * 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    index(req, res) {
        this.setSuccess(
            Response.HTTP_OK, 
            Response.statusText.HTTP_OK, 
            {
                timestamp: Number(new Date()),
                params: req.query 
            });
        return this.send(res);
    };

}

module.exports = homeController;