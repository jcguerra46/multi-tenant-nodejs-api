const db = require('../models/index');
const bcrypt = require('bcrypt');
const ApiController = require('../utilities/ApiController');
const HttpResponse = require("../utilities/HttpResponse");
const Response = new HttpResponse();

const AuthService = require('../services/authService');

class authController extends ApiController {

    /**
   * @route POST /auth/login
   * @desc Login user and return JWT token
   * @access Public
   *
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  async login(req, res) {
    try {
      let user = await AuthService.login(req, res);
      if (!user) {
        this.setError(
          Response.HTTP_NOT_FOUND, 
          `The invalid email or is not associated with a user.`
        );
        return this.send(res);
      }
      const { password } = req.body;
      let comparePassword = bcrypt.compareSync(password, user.password);
      if (!comparePassword) {
        this.setError(Response.HTTP_NOT_FOUND, "Incorrect password");
        return this.send(res);
      }

      this.setSuccess(Response.HTTP_OK, Response.statusText.HTTP_OK, {
        token: await AuthService.generateJWT(user),
        user
      });
      return this.send(res);
    } catch (error) {
        this.setError(Response.HTTP_UNPROCESSABLE_ENTITY, { errors: error.message });
        return this.send(res);
    }
  }

}

module.exports = authController;