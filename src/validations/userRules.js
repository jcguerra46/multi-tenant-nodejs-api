const { body, check, param } = require('express-validator');
const db = require('../models/index');

/**
 * Store rules validations
 * 
 * @returns 
 */
const userStoreRules = () => {

    return [
        check('firstName').notEmpty().withMessage('Your first name is required')
            .isString().withMessage('not string'),

        check('lastName').notEmpty().withMessage('Your last name is required')
            .isString().withMessage('not string'),

        check('email').isEmail().withMessage('Enter a valid email address')
            .exists().notEmpty().withMessage('Email is required')
            .isLength({max:255}).withMessage('max 255'),

        check('password').notEmpty().withMessage('Your password is required')
            .isLength({min:8}).withMessage('min 8 characters'),

        check('email').custom((value) => {
            return db.User.findOne({
                where: { email: value }
            }).then((user) => {
                if (user) {
                    return Promise.reject('E-mail already in use');
                }
            });
        }),

        body('passwordConfirmation').custom((value, { req }) => {
            if (value !== req.body.password) {
              throw new Error('Password confirmation does not match password');
            }
            return true;
        }),
    ]
};

/**
 * Update rules validations
 * 
 * @returns 
 */
const userUpdateRules = () => {

    return [
        param('id').notEmpty()
            .withMessage('User id param is required')
            .isInt().withMessage('User id param is not integer'),

        body('first_name').optional({ checkFalsy: true })
            .notEmpty().withMessage('You first name is required')
            .isString().withMessage('not string'),

        body('lastName').optional({ checkFalsy: true })
            .notEmpty().withMessage('Your last name is required')
            .isString().withMessage('not string'),

        body('email').optional({ checkFalsy: true })
            .notEmpty().withMessage('Email is required')
            .isEmail().withMessage('Enter a valid email address')
            .exists()
            .isLength({max:255}).withMessage('max 255'),

        body('password').optional({ checkFalsy: true })
            .notEmpty().withMessage('Your password is required')
            .isLength({min:8}).withMessage('min 8 characters'),

        body('email').optional({ checkFalsy: true }).notEmpty().custom((value) => {
            return db.User.findOne({
                where: { email: value }
            }).then((user) => {
                if (user) {
                    return Promise.reject('E-mail already in use');
                }
            });
        })
    ]
}

module.exports = {
    userStoreRules,
    userUpdateRules,
}
