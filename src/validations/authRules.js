const { body, param } = require('express-validator');

const loginRulesAuth = () => {
    return [
        body('email').notEmpty().withMessage('Email is required')
            .isEmail().withMessage('Enter a valid email address')
            .isLength({max:255}).withMessage('max 255'),

        body('password').notEmpty().isLength({min: 8})
            .withMessage('Must be at least 8 chars long'),
    ]
};

module.exports = {
    loginRulesAuth,
};