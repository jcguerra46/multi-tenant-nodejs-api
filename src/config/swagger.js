const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
    openapi: "3.0.0",
    info: {
      // API informations (required)
      title: 'Multi-tenant NodeJS API - Documentation', // Title (required)
      version: '1.0.0', // Version (required)
      description: 'Multi-tenant NodeJS API', // Description (optional)
    },
    host: `localhost:${process.env.APP_PORT || 3000}`, // Host (optional)
    basePath: '/', // Base path (optional)
    components: {
        securitySchemes: {
            BearerAuth: {
              type: 'http',
              scheme: 'bearer',
              bearerFormat: 'JWT'
            }
        }
    },
};
  
// Options for the swagger docs
const options = {
    // Import swaggerDefinitions
    swaggerDefinition,
    // Path to the API docs
    // Note that this path is relative to the current directory from which the Node.js is ran, not the application itself.
    apis: ['./src/routes/*.js'],
};

const swaggerSpect = swaggerJSDoc(options);

module.exports = {
    swaggerSpect,
}
