'use strict';

const faker = require('faker');
const bcrypt = require('bcrypt');

const users = [...Array(50)].map((user) => (
  {
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    email: faker.internet.email(),
    password: bcrypt.hashSync('password', bcrypt.genSaltSync(10), null),
    avatar: faker.internet.avatar(),
    companyId: faker.random.arrayElement([1,2,3,4,5,6,7,8,9,10]),
    createdAt: new Date(),
    updatedAt: new Date()
  }
));

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  
    // Admin User
    await queryInterface.bulkInsert('Users', [{
      firstName: 'John',
      lastName: 'War',
      email: 'admin@war.com',
      password:  bcrypt.hashSync('password', bcrypt.genSaltSync(10), null),
      avatar: faker.internet.avatar(),
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});

   // Users 
   await queryInterface.bulkInsert('Users', users, {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Users', null, {});
  }
};
