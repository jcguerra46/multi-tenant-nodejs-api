/**
 *  Class       : HttpResponse
 *  Description : Standard HTTP Response Codes and Status Texts.
 *  Usage       : HttpResponse = require("<path>/HttpResponse");
 *                const Response = new HttpResponse();
 *              
 *                console.log(Response.HTTP_OK);                  // 200
 *                console.log(Response.statusText.HTTP_OK);       // Ok
 */
class HttpResponse {

    constructor() {
  
      // RESPONSE CODES
      this.HTTP_CONTINUE = 100;
      this.HTTP_SWITCHING_PROTOCOLS = 101;
      this.HTTP_PROCESSING = 102;                             // RFC2518
      this.HTTP_OK = 200;
      this.HTTP_CREATED = 201;
      this.HTTP_ACCEPTED = 202;
      this.HTTP_NON_AUTHORITATIVE_INFORMATION = 203;
      this.HTTP_NO_CONTENT = 204;
      this.HTTP_RESET_CONTENT = 205;
      this.HTTP_PARTIAL_CONTENT = 206;
      this.HTTP_MULTI_STATUS = 207;                           // RFC4918
      this.HTTP_ALREADY_REPORTED = 208;                       // RFC5842
      this.HTTP_IM_USED = 226;                                // RFC3229
      this.HTTP_MULTIPLE_CHOICES = 300;
      this.HTTP_MOVED_PERMANENTLY = 301;
      this.HTTP_FOUND = 302;
      this.HTTP_SEE_OTHER = 303;
      this.HTTP_NOT_MODIFIED = 304;
      this.HTTP_USE_PROXY = 305;
      this.HTTP_RESERVED = 306;
      this.HTTP_TEMPORARY_REDIRECT = 307;
      this.HTTP_PERMANENTLY_REDIRECT = 308;                   // RFC7238
      this.HTTP_BAD_REQUEST = 400;
      this.HTTP_UNAUTHORIZED = 401;
      this.HTTP_PAYMENT_REQUIRED = 402;
      this.HTTP_FORBIDDEN = 403;
      this.HTTP_NOT_FOUND = 404;
      this.HTTP_METHOD_NOT_ALLOWED = 405;
      this.HTTP_NOT_ACCEPTABLE = 406;
      this.HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
      this.HTTP_REQUEST_TIMEOUT = 408;
      this.HTTP_CONFLICT = 409;
      this.HTTP_GONE = 410;
      this.HTTP_LENGTH_REQUIRED = 411;
      this.HTTP_PRECONDITION_FAILED = 412;
      this.HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
      this.HTTP_REQUEST_URI_TOO_LONG = 414;
      this.HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
      this.HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
      this.HTTP_EXPECTATION_FAILED = 417;
      this.HTTP_I_AM_A_TEAPOT = 418;                          // RFC2324
      this.HTTP_MISDIRECTED_REQUEST = 421;                    // RFC7540
      this.HTTP_UNPROCESSABLE_ENTITY = 422;                   // RFC4918
      this.HTTP_LOCKED = 423;                                 // RFC4918
      this.HTTP_FAILED_DEPENDENCY = 424;                      // RFC4918
      this.HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL = 425;   // RFC2817
      this.HTTP_UPGRADE_REQUIRED = 426;                       // RFC2817
      this.HTTP_PRECONDITION_REQUIRED = 428;                  // RFC6585
      this.HTTP_TOO_MANY_REQUESTS = 429;                      // RFC6585
      this.HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE = 431;        // RFC6585
      this.HTTP_UNAVAILABLE_FOR_LEGAL_REASONS = 451;
      this.HTTP_INTERNAL_SERVER_ERROR = 500;
      this.HTTP_NOT_IMPLEMENTED = 501;
      this.HTTP_BAD_GATEWAY = 502;
      this.HTTP_SERVICE_UNAVAILABLE = 503;
      this.HTTP_GATEWAY_TIMEOUT = 504;
      this.HTTP_VERSION_NOT_SUPPORTED = 505;
      this.HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL = 506;   // RFC2295
      this.HTTP_INSUFFICIENT_STORAGE = 507;                   // RFC4918
      this.HTTP_LOOP_DETECTED = 508;                          // RFC5842
      this.HTTP_NOT_EXTENDED = 510;                           // RFC2774
      this.HTTP_NETWORK_AUTHENTICATION_REQUIRED = 511;        // RFC6585
  
      // STATUS TEXTS
      this.statusText = {
  
        HTTP_CONTINUE: 'Continue',
        HTTP_SWITCHING_PROTOCOLS: 'Switching Protocols',
        HTTP_PROCESSING: 'Processing',                        // RFC2518
        HTTP_OK: 'OK',
        HTTP_CREATED: 'Created',
        HTTP_ACCEPTED: 'Accepted',
        HTTP_NON_AUTHORITATIVE_INFORMATION: 'Non-Authoritative Information',
        HTTP_NO_CONTENT: 'No Content',
        HTTP_RESET_CONTENT: 'Reset Content',
        HTTP_PARTIAL_CONTENT: 'Partial Content',
        HTTP_MULTI_STATUS: 'Multi-Status',                    // RFC4918
        HTTP_ALREADY_REPORTED: 'Already Reported',            // RFC5842
        HTTP_IM_USED: 'IM Used',                              // RFC3229
        HTTP_MULTIPLE_CHOICES: 'Multiple Choices',
        HTTP_MOVED_PERMANENTLY: 'Moved Permanently',
        HTTP_FOUND: 'Found',
        HTTP_SEE_OTHER: 'See Other',
        HTTP_NOT_MODIFIED: 'Not Modified',
        HTTP_USE_PROXY: 'Use Proxy',
        HTTP_RESERVED: 'Reserved',
        HTTP_TEMPORARY_REDIRECT: 'Temporary Redirect',
        HTTP_PERMANENTLY_REDIRECT: 'Permanent Redirect',      // RFC7238
        HTTP_BAD_REQUEST: 'Bad Request',
        HTTP_UNAUTHORIZED: 'Unauthorized',
        HTTP_PAYMENT_REQUIRED: 'Payment Required',
        HTTP_FORBIDDEN: 'Forbidden',
        HTTP_NOT_FOUND: 'Not Found',
        HTTP_METHOD_NOT_ALLOWED: 'Method Not Allowed',
        HTTP_NOT_ACCEPTABLE: 'Not Acceptable',
        HTTP_PROXY_AUTHENTICATION_REQUIRED: 'Proxy Authentication Required',
        HTTP_REQUEST_TIMEOUT: 'Request Timeout',
        HTTP_CONFLICT: 'Conflict',
        HTTP_GONE: 'Gone',
        HTTP_LENGTH_REQUIRED: 'Length Required',
        HTTP_PRECONDITION_FAILED: 'Precondition Failed',
        HTTP_REQUEST_ENTITY_TOO_LARGE: 'Request Entity Too Large',
        HTTP_REQUEST_URI_TOO_LONG: 'URI Too Long',
        HTTP_UNSUPPORTED_MEDIA_TYPE: 'Unsupported Media Type',
        HTTP_REQUESTED_RANGE_NOT_SATISFIABLE: 'Range Not Satisfiable',
        HTTP_EXPECTATION_FAILED: 'Expectation Failed',
        HTTP_I_AM_A_TEAPOT: 'I\'m a teapot',                  // RFC2324
        HTTP_MISDIRECTED_REQUEST: 'Misdirected Request',      // RFC7540
        HTTP_UNPROCESSABLE_ENTITY: 'Unprocessable Entity',    // RFC4918
        HTTP_LOCKED: 'Locked',                                // RFC4918
        HTTP_FAILED_DEPENDENCY: 'Failed Dependency',          // RFC4918
        HTTP_RESERVED_FOR_WEBDAV_ADVANCED_COLLECTIONS_EXPIRED_PROPOSAL: 'Reserved for WebDAV advanced collections expired proposal',   // RFC2817
        HTTP_UPGRADE_REQUIRED: 'Upgrade Required',            // RFC2817
        HTTP_PRECONDITION_REQUIRED: 'Precondition Required',  // RFC6585
        HTTP_TOO_MANY_REQUESTS: 'Too Many Requests',          // RFC6585
        HTTP_REQUEST_HEADER_FIELDS_TOO_LARGE: 'Request Header Fields Too Large',                             // RFC6585
        HTTP_UNAVAILABLE_FOR_LEGAL_REASONS: 'Unavailable For Legal Reasons',                               // RFC7725
        HTTP_INTERNAL_SERVER_ERROR: 'Internal Server Error',
        HTTP_NOT_IMPLEMENTED: 'Not Implemented',
        HTTP_BAD_GATEWAY: 'Bad Gateway',
        HTTP_SERVICE_UNAVAILABLE: 'Service Unavailable',
        HTTP_GATEWAY_TIMEOUT04: 'Gateway Timeout',
        HTTP_VERSION_NOT_SUPPORTED: 'HTTP Version Not Supported',
        HTTP_VARIANT_ALSO_NEGOTIATES_EXPERIMENTAL: 'Variant Also Negotiates',                                     // RFC2295
        HTTP_INSUFFICIENT_STORAGE: 'Insufficient Storage',    // RFC4918
        HTTP_LOOP_DETECTED: 'Loop Detected',                  // RFC5842
        HTTP_NOT_EXTENDED: 'Not Extended',                    // RFC2774
        HTTP_NETWORK_AUTHENTICATION_REQUIRED: 'Network Authentication Required',                             // RFC6585
      }
    }
  }
  
  module.exports = HttpResponse;