const db = require('../models/index');
const jwt = require('jsonwebtoken');

class AuthService {

    /**
     * Auth Login
     * 
     * @param {*} req 
     * @param {*} res 
     * @returns 
     */
    static async login(req, res) {
        try {
            const { email } = req.body;
            return  await db.User.findOne({
                where: { email }
            });
        } catch (error) {
            throw error;
        }
    }

    /**
     * Auth Generate JWT
     * 
     * @param {*} user 
     * @returns 
     */
    static async generateJWT(user) {
        const today = new Date();
        const expirationDate = new Date(today);
        expirationDate.setDate(today.getDate() + 60);

        let payload = {
            id: user.id,
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
        };

        return jwt.sign(payload, process.env.JWT_SECRET, {
            expiresIn: parseInt(expirationDate.getTime() / 1000, 10)
        });
    }
}

module.exports = AuthService;